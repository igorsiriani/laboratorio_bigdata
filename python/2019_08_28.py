def transcription(dna):
    mapping = {
        "A": "U",
        "T": "A",
        "C": "G",
        "G": "C"
    }

    return "".join([mapping.get(ch) for ch in dna])


def complement(dna_comp):
    mapping = {
        "A": "T",
        "T": "A",
        "C": "G",
        "G": "C"
    }

    return "".join([mapping.get(ch) for ch in dna_comp])


def transcription_back(rna):
    mapping = {
        "U": "A",
        "A": "T",
        "G": "C",
        "C": "G"
    }

    return "".join([mapping.get(ch) for ch in rna])


def menu(option1, sequence, option2):

    if option1 == 1 and option2 == 1:
        return sequence
    elif option1 == 1 and option2 == 2:
        return complement(sequence)
    elif option1 == 1 and option2 == 3:
        return transcription(complement(sequence))
    elif option1 == 2 and option2 == 1:
        return complement(sequence)
    elif option1 == 2 and option2 == 2:
        return sequence
    elif option1 == 2 and option2 == 3:
        return transcription(sequence)
    elif option1 == 3 and option2 == 1:
        return complement(transcription_back(sequence))
    elif option1 == 3 and option2 == 2:
        return transcription_back(sequence)
    elif option1 == 3 and option2 == 3:
        return sequence


def main():
    option1 = input("Qual sequencia esta sendo passada? \n  1. Codante\n  2. Complementar\n  3. RNA\n")
    sequence = input("Sequencia: ")
    option2 = input("Qual sequencia deseja encontrar? \n  1. Codante\n  2. Complementar\n  3. RNA\n")

    print("\n\nSequencia resultante: ", menu(int(option1), sequence, int(option2)))


if __name__ == '__main__':
    main()
