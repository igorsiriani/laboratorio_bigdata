from Bio import SeqIO


def genome_comparision():

    fasta_sequences1 = SeqIO.parse(open('./archives/Homo_sapiens.fasta'), 'fasta')
    fasta_sequences2 = SeqIO.parse(open('./archives/Pongo_abelii.fasta'), 'fasta')

    for fasta1 in fasta_sequences1:
        for fasta2 in fasta_sequences2:
            name1, sequence1 = fasta1.id, str(fasta1.seq)
            name2, sequence2 = fasta2.id, str(fasta2.seq)
            return comparision(sequence1, sequence2)


    # with open('./data/Salary_Data.csv', 'r') as csvFile:
    #     reader = csv.reader(csvFile)
    #     for row in reader:
    #         x.append(float(row[0]))
    #         y.append(float(row[1]))


def comparision(sequence1, sequence2):
    count = 0

    for i in range(len(sequence1)):
        try:
            if sequence1[i] == sequence2[i]:
                count += 1
        except Exception as e:
            print(e)
            break

    return (count * 100) / len(sequence1), count, len(sequence1)


def main():
    sequence1 = 'ACTGCTG'
    sequence2 = 'AACGCTG'

    percentage, equal, total = genome_comparision()
    # genome_comparision()
    print('Total = ' + str(total) + '\nIguais = ' + str(equal) + '\nPorcentagem = ' + str(percentage) + '%')


if __name__ == '__main__':
    main()
